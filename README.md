Streetfood
==========
Nomadic food is application for foodies in Pune.

We have gathered information about most famous street food stalls around pune.

Features:

-List View : You can get information,address,user ratings.

-Map View: Location,Driving directions of your food stalls.

-We can get nearest famous street shops with your gps location.

-We list stalls which cost you no more than Rs.100 .No costly restaurents.

-You can review ratings on Quality ,Hygeine,Customer Service.

So welcome aboard.

This application designed and developed by Sandeep Jagtap & Ishan Trikha.